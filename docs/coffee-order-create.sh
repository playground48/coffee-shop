# Place an order by customer of particular shop with for a selected item
# http://localhost:8080/order-service/order
curl -v -X POST -H "Content-type: application/json" http://localhost:8080/order-service/order -d '{"shopId": 1, "customerId": 1, "itemId": 1, "quantity": 1}'