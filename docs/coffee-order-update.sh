# Update the existing order
# http://localhost:8080/order-service/order/{orderId}
curl -v -X PUT -H "Content-type: application/json" http://localhost:8080/order-service/order/1 -d '{"itemId": 1, "quantity": 1}'