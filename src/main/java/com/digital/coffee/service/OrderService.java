package com.digital.coffee.service;

import com.digital.coffee.entity.ApiOrder;
import com.digital.coffee.model.Order;
import com.digital.coffee.util.exception.DataNotFoundException;
import com.digital.coffee.util.exception.InvalidParamException;

import java.util.List;

public interface OrderService {

    List<Order> getShopOrders(Long shopId) throws InvalidParamException;

    Order getOrder(Long orderId) throws InvalidParamException, DataNotFoundException;

    Order placeOrder(ApiOrder apiOrder) throws DataNotFoundException;

    Order updateOrder(Long orderId, ApiOrder apiOrder) throws InvalidParamException, DataNotFoundException;

    void cancelOrder(Long orderId) throws InvalidParamException, DataNotFoundException;
}
