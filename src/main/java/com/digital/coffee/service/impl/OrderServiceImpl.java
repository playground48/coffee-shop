package com.digital.coffee.service.impl;

import com.digital.coffee.entity.ApiOrder;
import com.digital.coffee.model.Customer;
import com.digital.coffee.model.Item;
import com.digital.coffee.model.Order;
import com.digital.coffee.model.Shop;
import com.digital.coffee.repo.CustomerRepository;
import com.digital.coffee.repo.ItemRepository;
import com.digital.coffee.repo.OrderRepository;
import com.digital.coffee.repo.ShopRepository;
import com.digital.coffee.service.OrderService;
import com.digital.coffee.util.exception.DataNotFoundException;
import com.digital.coffee.util.exception.InvalidParamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private static Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<Order> getShopOrders(Long shopId) throws InvalidParamException {

        if (shopId == null) {
            logger.info("Shop id not found");
            throw new InvalidParamException("Shop id required");
        }
        return orderRepository.findByShop_Id(shopId);
    }

    @Override
    public Order getOrder(Long orderId) throws InvalidParamException, DataNotFoundException {

        if (orderId == null) {
            logger.info("Order id not found");
            throw new InvalidParamException("Order id required");
        }
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new DataNotFoundException("Order not found of order id " + orderId));
    }

    @Override
    public Order placeOrder(ApiOrder apiOrder) throws DataNotFoundException {

        if (apiOrder == null || apiOrder.getShopId() == null || apiOrder.getCustomerId() == null || apiOrder.getItemId() == null) {
            logger.info("Order request required data not found {}", apiOrder);
            throw new DataNotFoundException("Request not contains required data");
        }
        Order order = new Order();
        order.setOrderStatus(Order.OrderStatus.ORDERED);
        order.setOrderDateTime(LocalDateTime.now());
        order.setCustomer(getCustomer(apiOrder));
        order.setItem(getItem(apiOrder));
        order.setShop(getShop(apiOrder));
        if (apiOrder.getQuantity() <= 0) {
            apiOrder.setQuantity(1);
        }
        order.setQuantity(apiOrder.getQuantity());
        String orderNumber = String.valueOf(System.currentTimeMillis());
        order.setOrderNumber(orderNumber);
        logger.info("Order placing {} order number {}", apiOrder, orderNumber);
        return orderRepository.save(order);
    }

    @Override
    public Order updateOrder(Long orderId, ApiOrder apiOrder) throws InvalidParamException, DataNotFoundException {
        Order order = getOrder(orderId);
        order.setItem(getItem(apiOrder));
        if (apiOrder.getQuantity() <= 0) {
            apiOrder.setQuantity(1);
        }
        order.setQuantity(apiOrder.getQuantity());
        order.setOrderDateTime(LocalDateTime.now());
        logger.info("Order has been updated [{}]", order.getOrderNumber());
        return orderRepository.save(order);
    }

    @Override
    public void cancelOrder(Long orderId) throws InvalidParamException, DataNotFoundException {
        Order order = getOrder(orderId);
        logger.info("Order has been canceled [{}]", order.getOrderNumber());
        order.setOrderStatus(Order.OrderStatus.CANCELED);
        orderRepository.save(order);
    }

    private Customer getCustomer(ApiOrder apiOrder) throws DataNotFoundException {
        return customerRepository.findById(apiOrder.getCustomerId())
                .orElseThrow(() -> new DataNotFoundException("Customer not found of id " + apiOrder.getCustomerId()));
    }

    private Item getItem(ApiOrder apiOrder) throws DataNotFoundException {
        return itemRepository.findById(apiOrder.getItemId())
                .orElseThrow(() -> new DataNotFoundException("Item not found of id " + apiOrder.getItemId()));
    }

    private Shop getShop(ApiOrder apiOrder) throws DataNotFoundException {
        return shopRepository.findById(apiOrder.getShopId())
                .orElseThrow(() -> new DataNotFoundException("Shop not found of id " + apiOrder.getShopId()));
    }
}
