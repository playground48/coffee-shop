package com.digital.coffee.repo;

import com.digital.coffee.model.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByShop_Id(Long shopId);
}
