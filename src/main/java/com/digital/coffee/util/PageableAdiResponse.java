package com.digital.coffee.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class PageableAdiResponse extends ApiResponse {

    private PageData pageData = new PageData();

    public PageableAdiResponse(Object payload) {
        super(payload);
    }

    public PageableAdiResponse(StatusCode statusCode, Object payload) {
        super(statusCode, payload);
    }

    public PageableAdiResponse(StatusCode statusCode, Object payload, String debugMessage) {
        super(statusCode, payload, debugMessage);
    }

    public static PageableAdiResponse getPageInstance(Page page) {
        PageableAdiResponse response = new PageableAdiResponse(page.getContent());
        response.setPageData(new PageData(page.getTotalPages(), page.getNumber(), page.getTotalElements()));
        return response;
    }
}
