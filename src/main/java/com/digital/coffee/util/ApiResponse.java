package com.digital.coffee.util;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static com.digital.coffee.util.SystemKey.DD_MM_YYYY_HH_MM_SS;

@Getter
@Setter
public class ApiResponse {

    public ApiResponse(Object payload) {
        this.setStatusCode(StatusCode.S1000);
        this.setStatusMessage(StatusCode.S1000.getDescription());
        this.payload = payload;
    }

    public ApiResponse(StatusCode statusCode, Object payload) {
        this.setStatusCode(statusCode);
        this.setStatusMessage(statusCode.getDescription());
        this.payload = payload;
    }

    public ApiResponse(StatusCode statusCode, Object payload, String debugMessage) {
        this.setStatusCode(statusCode);
        this.setStatusMessage(statusCode.getDescription());
        this.setDebugMessage(debugMessage);
        this.payload = payload;
    }

    private StatusCode statusCode;
    private String statusMessage;
    private String debugMessage;
    private Object payload;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DD_MM_YYYY_HH_MM_SS)
    private LocalDateTime timestamp = LocalDateTime.now();
}
