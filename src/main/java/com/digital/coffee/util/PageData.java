package com.digital.coffee.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageData {

    public PageData() {
    }

    public PageData(int totalPages, int pageNumber, long totalElements) {
        this.totalPages = totalPages;
        this.pageNumber = pageNumber;
        this.totalElements = totalElements;
    }

    private int totalPages;
    private int pageNumber;
    private long totalElements;

}
