package com.digital.coffee.util;

public enum StatusCode {

    S1000("Success"),
    S1001("Accepted"),
    E1000("System error occurred"),
    E1001("Request params not accepted"),
    E1002("Request data not found");

    private String description;

    StatusCode(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
