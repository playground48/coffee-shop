package com.digital.coffee.util.exception;

import com.digital.coffee.util.StatusCode;

public class InvalidParamException extends Exception {

    public InvalidParamException() {
    }

    public InvalidParamException(String message) {
        super(message);
    }

    public InvalidParamException(String message, Throwable cause) {
        super(message, cause);
    }

    public StatusCode getStatusCode() {
        return StatusCode.E1001;
    }
}
