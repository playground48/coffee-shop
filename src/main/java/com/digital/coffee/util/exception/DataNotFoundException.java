package com.digital.coffee.util.exception;

import com.digital.coffee.util.StatusCode;

public class DataNotFoundException extends Exception {

    public DataNotFoundException() {
    }

    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public StatusCode getStatusCode() {
        return StatusCode.E1002;
    }
}
