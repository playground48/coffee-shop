package com.digital.coffee.model;

import com.digital.coffee.util.SystemKey;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "order_item")
@Getter
@Setter
public class Order implements Serializable {

    public enum OrderStatus {
        ORDERED, PURCHASED, CANCELED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private int quantity;
    @Column
    private String orderNumber;
    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = SystemKey.DD_MM_YYYY_HH_MM_SS)
    @DateTimeFormat(pattern = SystemKey.DD_MM_YYYY_HH_MM_SS)
    private LocalDateTime orderDateTime;
    @Column
    private OrderStatus orderStatus;
    @JoinColumn
    @ManyToOne
    private Item item;
    @JoinColumn
    @ManyToOne
    private Shop shop;
    @JoinColumn
    @ManyToOne
    private Customer customer;
}
