package com.digital.coffee.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApiOrder {

    private Long shopId;
    private Long customerId;
    private Long itemId;
    private int quantity;
}
