package com.digital.coffee.controller;

import com.digital.coffee.entity.ApiOrder;
import com.digital.coffee.model.Order;
import com.digital.coffee.service.OrderService;
import com.digital.coffee.util.ApiResponse;
import com.digital.coffee.util.exception.DataNotFoundException;
import com.digital.coffee.util.exception.InvalidParamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/order-service")
public class OrderController {

    private static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    /**
     * Get all list of orders available on particular shop.
     * @param shopId
     * @return
     */
    @GetMapping("/shop/{shopId}/orders")
    public ResponseEntity<ApiResponse> getShopItemOrderList(@PathVariable("shopId") Long shopId) {
        try {
            logger.info("Loading orders for shop id [{}]", shopId);
            List<Order> orders = orderService.getShopOrders(shopId);
            return ResponseEntity.ok(new ApiResponse(orders));
        } catch (InvalidParamException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), new ArrayList<>(), e.getLocalizedMessage()));
        }
    }

    /**
     * Get particular order details on a shop via orderId
     * @param orderId
     * @return
     */
    @GetMapping("/order/{orderId}")
    public ResponseEntity<ApiResponse> getCustomerShopOrderItemList(@PathVariable("orderId") Long orderId) {
        try {
            logger.info("Loading order of id [{}]", orderId);
            Order order = orderService.getOrder(orderId);
            return ResponseEntity.ok(new ApiResponse(order));
        } catch (DataNotFoundException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), null, e.getMessage()));
        } catch (InvalidParamException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), null, e.getMessage()));
        }
    }

    /**
     * Place an order via api with the selected shop. If order quantity is 0 or less than, then system place the order
     * with quantity to 1. Parameter entity {@link ApiOrder}
     * @param apiOrder
     * @return
     */
    @PostMapping("/order")
    public ResponseEntity<ApiResponse> placeOrder(@RequestBody ApiOrder apiOrder) {
        try {
            logger.info("Order place request {}", apiOrder);
            Order order = orderService.placeOrder(apiOrder);
            return ResponseEntity.ok(new ApiResponse(order));
        } catch (DataNotFoundException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), null, e.getMessage()));
        }
    }

    /**
     * Update the already placed order. It's only able to update order quantity and item.
     * @param orderId
     * @param apiOrder
     * @return
     */
    @PutMapping("/order/{orderId}")
    public ResponseEntity<ApiResponse> placeOrder(@PathVariable("orderId") Long orderId, @RequestBody ApiOrder apiOrder) {
        try {
            logger.info("Order updating request order id [{}], param [{}]", orderId, apiOrder);
            Order order = orderService.updateOrder(orderId, apiOrder);
            return ResponseEntity.ok(new ApiResponse(order));
        } catch (DataNotFoundException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), null, e.getMessage()));
        } catch (InvalidParamException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), null, e.getMessage()));
        }
    }

    /**
     * Delete/cancel the existing order
     * @param orderId
     * @return
     */
    @DeleteMapping("/order/{orderId}")
    public ResponseEntity<ApiResponse> removeOrder(@PathVariable("orderId") Long orderId) {
        try {
            logger.info("Order cancelling request {}", orderId);
            orderService.cancelOrder(orderId);
            return ResponseEntity.ok(new ApiResponse(null));
        } catch (DataNotFoundException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), null, e.getMessage()));
        } catch (InvalidParamException e) {
            logger.error("Error on request", e);
            return ResponseEntity.ok(new ApiResponse(e.getStatusCode(), null, e.getMessage()));
        }
    }
}
