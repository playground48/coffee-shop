package com.digital.coffee;

import com.digital.coffee.util.ApiResponse;
import com.digital.coffee.util.StatusCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CommonExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<?> exception(final Throwable throwable) {

        String errorMessage = (throwable != null ? throwable.getMessage() : "HTTP error occurred");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ApiResponse(StatusCode.E1000, null, errorMessage));
    }
}
