create table customer (id  bigserial not null, contact_number varchar(255), name varchar(255), primary key (id));
create table item (id  bigserial not null, item_code varchar(255), item_name varchar(255), price DECIMAL(10,2), primary key (id));
create table order_item (id  bigserial not null, order_date_time timestamp, order_number varchar(255), order_status int4, quantity int4, customer_id int8, item_id int8, shop_id int8, primary key (id));
create table shop (id  bigserial not null, address varchar(255), shop_name varchar(255), primary key (id));

alter table order_item add constraint FK983cvgn2yeo3724p74o49hqge foreign key (customer_id) references customer;
alter table order_item add constraint FKija6hjjiit8dprnmvtvgdp6ru foreign key (item_id) references item;
alter table order_item add constraint FKm82xhxu7jg5am1b58v61c32xx foreign key (shop_id) references shop;