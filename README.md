# Coffee Application Service

- Coffee shop pre order application service for order management via API service

## System requirement

- Spring boot - 2.3.4.RELEASE
- Postgresql - 9.6-alpine
- Jdk - 11
- docker
- docker-compose
- Maven 3.6.*

## Application build & deployment guide

- Follow the below steps to setup the service on docker.

1. Build in maven
> mvn package -DskipTests

2. Build docker image
> docker build -t coffee-shop .

3. Run services via docker-compose
- NOTE: Please check docker-compose.yml for necessary configurations
> docker-compose up

```
Please note that there is a scipt buid-docker.sh which able to run above 3 steps in one.
```

## Application service test

1. Test in linux bash with curl
> Please use the scripts on "docs" directory to send API requests.

- docs/coffee-order-create.sh
- docs/coffee-order-delete.sh
- docs/coffee-order-get.sh
- docs/coffee-order-list.sh
- docs/coffee-order-update.sh

2. Test via postman

> Import docs/CoffeeShop.postman_collection.json collection to setup APIs on postman.