#!/usr/bin/env bash
# Build in maven
mvn package -DskipTests
# build docker image
docker build -t coffee-shop .
# Run services via docker-compose
docker-compose up

# Check the docker postgres console via bash cli
# docker exec -it pgdb bash
# psql -Upostgres